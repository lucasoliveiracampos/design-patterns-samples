public class LinkText implements IDocumentPart {
    public String text;
    public String url;

    public LinkText(String text, String url){
        this.url = url; 
        this.text = text;
    }

    public String accept(IDocumentPartVisitor documentPartVisitor) {
        return documentPartVisitor.visit(this);
    }
}