public class Main {

    public static void main(String[] args) {
        Product water = new Product("Water", 1d);        
        Product beer = new Product("Beer", 2.5);        
        Product soda = new Product("Soda", 1.5);        

        Product[] products = {water, beer, soda};

        NoSales noSales = new NoSales();
        BlackFridaySales blackFridaySales = new BlackFridaySales();

        for (Product product : products) {
            System.out.println(String.format("Price for NoSales = %s", product.getRealPrice(noSales)));
            System.out.println(String.format("Price for BlackFridaySales = %s", product.getRealPrice(blackFridaySales)));
        }
    }
}