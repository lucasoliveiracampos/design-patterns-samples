public class PlainText implements IDocumentPart {

    public String content;

    public PlainText(String content) {
        this.content = content;
    }

    public String accept(IDocumentPartVisitor documentPartVisitor) {
        return documentPartVisitor.visit(this);
    }
} 