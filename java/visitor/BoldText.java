public class BoldText implements IDocumentPart {

    public String content;

    public BoldText(String content) {
        this.content = content;
    }

    public String accept(IDocumentPartVisitor documentPartVisitor) {
        return documentPartVisitor.visit(this);
    }
}