public interface IDocumentPart {
    public String accept(IDocumentPartVisitor documentPartVisitor);
}