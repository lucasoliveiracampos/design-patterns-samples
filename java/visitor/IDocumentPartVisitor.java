public interface IDocumentPartVisitor {

    public String visit(BoldText element);
    public String visit(PlainText element);
    public String visit(ItalicText element);
    public String visit(LinkText element);
}