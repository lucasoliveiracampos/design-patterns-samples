public class Product {
    public String name;
    public double price;

    public Product(String name, double price) {
        this.name = name;
        this.price = price;
    }

    public double getRealPrice(final ISales saleStrategy) {
        return price - (price * saleStrategy.getDiscount() / 100);
    }
}