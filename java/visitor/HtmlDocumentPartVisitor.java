public class HtmlDocumentPartVisitor implements IDocumentPartVisitor {

    public String visit(BoldText element) {
        return String.format("<b>%s</b>", element.content);
    }

    public String visit(PlainText element) {
        return element.content;
    }

    public String visit(ItalicText element) {
        return String.format("<i>%s</i>", element.content);
    }

    public String visit(LinkText element) {
        return String.format("<a src=%1s>%2s</a>", element.url, element.text);
    }

}