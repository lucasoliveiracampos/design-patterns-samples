public class ItalicText implements IDocumentPart {

    public String content;

    public ItalicText(String content) {
        this.content = content;
    }

    public String accept(IDocumentPartVisitor documentPartVisitor) {
        return documentPartVisitor.visit(this);
    }
}