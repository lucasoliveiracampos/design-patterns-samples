public class MarkdownDocumentPartVisitor implements IDocumentPartVisitor {

    public String visit(BoldText element) {
        return String.format("**%s**", element.content);
    }

    public String visit(PlainText element) {
        return element.content;
    }

    public String visit(ItalicText element) {
        return String.format("_%s_", element.content);
    }

    public String visit(LinkText element) {
        return String.format("[%1s](%2s)", element.text, element.url);
    }

}