/*
Step 1
You’ve just inherited a project with the following object structure:
    - An interface, DocumentPart, with a single method, toHTML(), which should return a String representing the DocumentPart in HTML format.
    - 5 implementations of DocumentPart: PlainText, BoldText, ItalicText, LineBreak and Image.

Step 2
The customer is now asking for the ability to output their documents to multiple formats including LaTex, Markdown and plaintext. How would you go about achieving this? 
*/

public class Main {

    public static void main(String[] args) {
        PlainText plainText = new PlainText("this is plain text");
        BoldText boldText = new BoldText("this is bold text");
        ItalicText italicText = new ItalicText("this is italic text");
        LinkText linkText = new LinkText("This is a link", "https://www.google.com");

        // IDocumentPart[] document = {plainText, boldText, italicText, lineBreak, image, linkText};
        IDocumentPart[] document = {plainText, boldText, italicText, linkText};
        IDocumentPartVisitor documentPartVisitor = new HtmlDocumentPartVisitor();
        //documentPartVisitor = new MarkdownDocumentPartVisitor();

        for (IDocumentPart documentPart : document) {
            System.out.println(documentPart.accept(documentPartVisitor));
        }

        System.out.println("======== NOW IN MARKDOWN ========");

        documentPartVisitor = new MarkdownDocumentPartVisitor();

        for (IDocumentPart documentPart : document) {
            System.out.println(documentPart.accept(documentPartVisitor));
        }


    }
}